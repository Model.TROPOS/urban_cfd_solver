﻿
# CAIRDIO v1.0
### city-scale air dispersion model with diffusive obstacles

## Overview

CAIRDIO is an atmospheric large-eddy simulation code for application on urban dispersion modeling. In CAIRDIO, buildings have an effect on the physical conservation laws of mass and momentum through modification of the permeability and capacity of grid cells. As such, CAIRDIO offers an enhanced flexibility in the choice of the grid resolution at the coarse end compared to standard obstalce-resolving codes. This includes marginally resolved building ensembles, i.e. with diffusive and overlapping building boundaries, for which urban dispersion can still be modeled with satisfactory detail but only for a fraction of the computation time needed to model fully-resolved obstacles. CAIRDIO uses terrain-fitted coordinates to describe a surface orography, and is therefore also suitable for the city-scale and beyond.
CAIRDIO is written in Python 2.7 and makes extensive use of the Python packages NumPy and SciPy for performance-critical model components. To improve the applicability on extensive domains and using modern hardware, CAIRDIO is parallelized using MPI.

## Description for the execution of a simple model run

### Requirements for a model run

  
#### The following software need to be installed first:

* Python version 2.7

* OPEN MPI, preferable latest release

* HDF5 C library, preferable latest release

* netCDF-4 C library, preferable latest release

  

#### CAIRDIO requires the following Python 2.7 packages to be available:

* NumPy

* SciPy

* netCDF4

* MPI for Python

* Shapely (for the user-costumized generation of grid files)

* Matplotlib (for the plotting of buildings)

It is to make sure if installed modules are located in the global default directory (i.e. site-packages directory).

Otherwise, the environmental variable `PYTHONPATH` needs to be set to the respective directory before each model run:

`export PYTHONPATH=$folder_to_your_python_modules`

### Preparation of the model setup

Each model setup is distinguished by the **simulation name**, which is the prefix of the different input files like grid file, driving files, and emission file all located in the relative directory `./INPUT/`. The simulation name is also the prefix of the output files located in `./OUTPUT/`. 

The **output folder** has to be manually created in advance of the simulation inside the main directory containing the main routine `pamirr.py`.

The **grid file** contains the gridded building-geometry information and surface orography. It's name follows the rule `${prefix}_grid.nc`

The **driving  files** contain predefined or interpolated 3-d fields which include initial or forcing fields of all prognostic variables, target turbulent intensities for the turbulence recycling scheme, and additionally the dispersed tracer variables, if non-trivial (homogeneos-zero Dirichlet) are used. The file names follow the convention `${prefix}_bnd_${time}.nc`, where time refers to a 6-digid integer indicating the seconds of elapsed simulation time at which the driving data is valid.

If dispersion simulations of test tracers are carried out, an **emission file** is needed, which contains the gridded 3-d information of sources for each tracer. Currently, only static emissions are supported, so there is no time dimension in the emission fields. In the emission file, the names of the test tracers are defined. So in the case if a tracer has no sources (because the tracer is advected into the domain trough the lateral boundaries), a zero field has to be nevertheless included in the emission file. Optionally, if only flow simulations are carried out, an emission file is not necessary. The emission file follows the convention `${prefix}_emiss.nc`.

To summarize, a complete set of input data may be composed of:
 * `play_ground_grid.nc`
 * `play_ground_bnd_000000.nc`
 * `play_ground_bnd_001000.nc`
 * `play_ground_bnd_002000.nc`
 * `play_ground_emiss.nc`
 

To **generate the input files** for a simple example, please enter the preprocessing folder (relative path `./PREPROC/`). `grid_gen.py` is an executable Python script to generate a grid file from a predefined building ensemble, which can be easily modified by adding/removing/replacing buildings in the building list `geoms`.  With the scripts `bound_gen*`  boundary conditions for 3 different scenarious are predefined (constant approaching wind, rotating wind, surface heating). Finally, the script `emiss_gen.py` creates an emission file for 2 tracers, one emitted trough a point source, and another trough an area source (emission into several grid cells).

After execution of the scripts, please do not forget to move the generated input files to `../INPUT/`.

 The **model settings** are listed in the namelist file `namelist.py` located in the main directory. The namelist file is itself a Python script which defines a dictionary that can be conviniently read in.  Some of the key namelist settings are:
  
   * `simulation_name`:    has to match the prefix of the input files. 
   * `npc`, `npr`: The product of the number of processes in both horizontal dimensions has to match the number of cores in the `mpirun` call, which have to be available on the executing system.
   * `st_time`:   has to be the same as in `bound_gen.py`.
   `sim_time`:           has to be less or equal as in `bound_gen.py`.
   `with_emissfile`:     has to be `True`, if an emission file is provided; otherwise only fluid dynamics is computed.
   `bnd_chem_*`:   has to be all `None`, if trivial boundary conditions are used for the tracer fields.

### Run the model

In order  to run the model, please execute the following command:

`mpirun -np $np python main.py`. 

Please make sure, `$np` matches the product of specified processes in the namelist file. 

> **Sitenote:**: There is also an advect-only routine named after `main_advect_only.py`, which considers tracer dispersion under static conditions, using a potential flow solution derived from the initial conditions.


In case of troubles, please feel free to contact the main developer of the model (weger@tropos.de).

